package com.metadesign.problem.controller;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metadesign.problem.model.Users;
import com.metadesign.problem.service.UserService;



@RestController
@RequestMapping("/api/v1/users")
public class UserController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	@Autowired 
	UserService userService;
	
	   @GetMapping("/allUsers")
	    public ResponseEntity<List<Users>> getAllUsers() {
		   
		   logger.info("Get All User Start :");
	        List<Users> list = userService.findAll();
	 
	        return new ResponseEntity<List<Users>>(list, new HttpHeaders(), HttpStatus.OK);
	    }
	 
	    @GetMapping("{id}")
	    public ResponseEntity<Users> getUsersById(@PathVariable("id") Long id){
	    logger.info("getUsersById Start :");                                              
	    	Users entity = userService.findById(id);
	 
	        return new ResponseEntity<Users>(entity, new HttpHeaders(), HttpStatus.OK);
	    }
	 

	    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
	    public ResponseEntity<Users> addUser(@RequestBody Users user)
	                                                    {
	    	  logger.info("addUser Start :" + user);
	    	  
	    	  Users u = new Users(user.getId(), user.getCreated_by(), user.getCreated_time(),
	      			user.getLast_updated_by(),
	      			user.getLast_updated_time(), 
	      			user.getFirst_name(), user.getLast_name(),
	      			user.getPicture(), user.getPreferred_language(), user.getBackground_check());
	    	  
	    	  logger.info("addUser details  : " + u);
	    	Users add = userService.insert(u);
	        return new ResponseEntity<Users>(add, new HttpHeaders(), HttpStatus.OK);
	    }
	 
	    @DeleteMapping("/{id}")
	    public HttpStatus deleteUser(@PathVariable("id") String id)
	               
	       
	    {
	    	
	    	logger.info("deleteUser Start :");
	    	userService.deleteById(id);
	    	
	    	 
	        return HttpStatus.OK;
	    }
	    
	    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	    public ResponseEntity<Users> UpdateUser(@RequestBody Users user)
	                                                    {
	    	logger.info("UpdateUser Start :");
	  	  
	    	  Users u = new Users(user.getId(), user.getCreated_by(), user.getCreated_time(),
	      			user.getLast_updated_by(),
	      			user.getLast_updated_time(), 
	      			user.getFirst_name(), user.getLast_name(),
	      			user.getPicture(), user.getPreferred_language(), user.getBackground_check());
	    	int userId = userService.update(u);
	    	
	    	logger.info("update user id  : " + userId);
	    	return new ResponseEntity<Users>(HttpStatus.OK);
	    }

}
