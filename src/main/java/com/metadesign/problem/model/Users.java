package com.metadesign.problem.model;

import java.util.Date;

public class Users {
	
	private String id;	
	private String created_by;	
	private Date created_time;	
	private String last_updated_by;	
	private Date last_updated_time;
	private String first_name;
	private String last_name;
	private String picture;
	private String preferred_language;
	private int background_check;
	
	public Users(String id, String created_by, Date created_time, String last_updated_by, Date last_updated_time,
			String first_name, String last_name, String picture, String preferred_language, int background_check) {
		super();
		this.id = id;
		this.created_by = created_by;
		this.created_time = created_time;
		this.last_updated_by = last_updated_by;
		this.last_updated_time = last_updated_time;
		this.first_name = first_name;
		this.last_name = last_name;
		this.picture = picture;
		this.preferred_language = preferred_language;
		this.background_check = background_check;
	}
	
	
	public Users() {
		
	}
	@Override
	public String toString() {
		return "Users [id=" + id + ", created_by=" + created_by + ", created_time=" + created_time
				+ ", last_updated_by=" + last_updated_by + ", last_updated_time=" + last_updated_time + ", first_name="
				+ first_name + ", last_name=" + last_name + ", picture=" + picture + ", preferred_language="
				+ preferred_language + ", background_check=" + background_check + "]";
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getCreated_by() {
		return created_by;
	}


	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}


	public Date getCreated_time() {
		return created_time;
	}


	public void setCreated_time(Date created_time) {
		this.created_time = created_time;
	}


	public String getLast_updated_by() {
		return last_updated_by;
	}


	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}


	public Date getLast_updated_time() {
		return last_updated_time;
	}


	public void setLast_updated_time(Date last_updated_time) {
		this.last_updated_time = last_updated_time;
	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getPicture() {
		return picture;
	}


	public void setPicture(String picture) {
		this.picture = picture;
	}


	public String getPreferred_language() {
		return preferred_language;
	}


	public void setPreferred_language(String preferred_language) {
		this.preferred_language = preferred_language;
	}


	public int getBackground_check() {
		return background_check;
	}


	public void setBackground_check(int background_check) {
		this.background_check = background_check;
	}
	
	
	
	

}
