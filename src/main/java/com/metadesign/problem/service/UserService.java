package com.metadesign.problem.service;

import java.util.List;

import javax.annotation.Resource;

import org.h2.engine.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.metadesign.problem.model.Users;
import com.metadesign.problem.repo.UserRepo;

@Service
public class UserService {
	
	
	 private Logger logger = LoggerFactory.getLogger(this.getClass());
	 
	 @Autowired 	
	UserRepo userRepo;
	

    public List < Users > findAll()
    {
    	
    return userRepo.findAll();	
    }


    public Users findById(long id) 
    
    {
    	
    	return userRepo.findById(id);
    	
    }

 
    public void deleteById(String id) 
    {
    	userRepo.deleteById(id);
    }
    
    public Users insert(Users user)
    {
    	logger.info("insert the start method:" + user);
    	
    	

    	
    	
    	return userRepo.insert(user);
    }


    public int update(Users user)
    {
    	
    	return userRepo.update(user);
    }
	
	

}
