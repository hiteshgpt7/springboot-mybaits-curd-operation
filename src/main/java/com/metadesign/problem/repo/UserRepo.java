package com.metadesign.problem.repo;


import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.metadesign.problem.model.Users;;

@Mapper
public interface UserRepo {

	
	@Select("select * from users")
    public List < Users > findAll();

    @Select("SELECT * FROM users WHERE id = #{id}")
    public Users findById(long id);

    @Delete("DELETE FROM users WHERE id = #{id}")
    public void deleteById(String id);

    @Insert("INSERT INTO users(id, created_by, created_time,last_updated_by,"
    		+ "last_updated_time,first_name,last_name,picture,preferred_language,background_check) " +
        " VALUES (#{id}, #{created_by}, #{created_time}, #{last_updated_by},#{last_updated_time},"
        + " #{first_name}, #{last_name}, #{picture}, #{preferred_language}, #{background_check})")
    public Users insert(Users user);

    @Update("Update users set first_name=#{first_name}, " +
        " last_name=#{last_name} where id=#{id}")
    public int update(Users users);
}
