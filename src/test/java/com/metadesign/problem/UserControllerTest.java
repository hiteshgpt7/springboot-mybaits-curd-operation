package com.metadesign.problem;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;


import com.metadesign.problem.model.Users;
import com.metadesign.problem.repo.UserRepo;
import com.metadesign.problem.service.UserService;




@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.MOCK, classes={ Application.class })
public class UserControllerTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}

	

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;

	@MockBean 
	private UserService userService;
	
	
	@InjectMocks
	UserRepo userRepo;
	@Before
	public void setUp() {
		 this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void addUser_test() throws Exception {
		
		
		Users u = new Users("1", "hitesh",new Date(),
      			"hitesh",
      			new Date(), 
      			"hitesh", "gupta",
      			"abc.jpg","EN", 0);
		
		when(userService.insert(u)).thenReturn(u);
	
		
		mockMvc.perform(post("/addUser")
			   .contentType(MediaType.APPLICATION_JSON)
			   .content("{\r\n \r\n \r\n\t\t\"id\": \"abc_1\",\r\n\t\t\"created_by\": \"hitesh\",\r\n\t\t\"created_time\": \"2020-04-05\",\r\n\t\t\"last_updated_by\": \"hitesh\",\r\n\t\t\"last_updated_time\": \"2020-04-05\",\r\n\t\t\"first_name\": \"hitesh\",\r\n\t\t\"last_name\": \"gupta\",\r\n\t\t\"picture\": \"abc\",\r\n\t\t\"preferred_language\": \"english\",\r\n\t\t\"background_check\": \"1\"\r\n}")						
			   .accept(MediaType.APPLICATION_JSON))
			   .andExpect(status().isCreated())
			   .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))		
			   .andExpect(jsonPath("$.id").value("1"));			   
			  	
	}
	
	
	@Test
	public void getUser_test() throws Exception {
		
		/* setup mock */
		Users u = new Users("1", "hitesh",new Date(),
      			"hitesh",
      			new Date(), 
      			"hitesh", "gupta",
      			"abc.jpg","EN", 0);
		when(userService.findById(1)).thenReturn(u);
		
		mockMvc.perform(get("/api/v1/users/1")				
			   .accept(MediaType.APPLICATION_JSON))
			   .andExpect(status().isOk())
			   .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
			   .andExpect(jsonPath("$.first_name").value("hitesh"));
			  
	}
}
